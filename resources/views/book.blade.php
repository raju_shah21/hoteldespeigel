<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>HotelDeSpiegel</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
       
      
    </head>
  
    <body style="background-color: #fcfcfc">
    





        <!--          navbar start -->
      
        @include('include.navbar')  

      <!--          navbar end -->





      <!--          hote image start -->
      
      <div class="inner-banner">
        <img src="img/slider/bg-1.jpg" alt="" width="100%" height="350px">

        <div class="inner-caption">
              <h1 class="text-info">Book Your Room</h1>
              <p>Home / Book</p>
            </div>
      </div>
        
          <!--          hotel image end -->




          <section class="booking-info m-5">
            <div class="container ">
              <form action="" method="get" class="mx-auto">
                <fieldset class="booking-room-detail">
                    <h3>Room Details</h3> 
                    <br>        
                             
                    <div class="form-group row">
                      <label class="col-form-label col-xl-2">Room Type</label>
                      <div class="col-xl-6">
                        <input type="text" readonly="" value="Standard Room Queen Bed (with A/C)" name="room_type" class="form-control">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-xl-2">Number of Rooms</label>
                      <div class="col-xl-6">
                        <select id="total_rooms" name="total_rooms" class="form-control"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>            </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-xl-2">Price (USD)</label>
                      <div class="col-xl-6">
                        <input type="text" readonly="" id="room_price" value="30" name="room_price" class="form-control" placeholder="Price per room">
                        
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-form-label col-xl-2">Total Price (USD)</label>
                      <div class="col-xl-6">
                        <input type="text" id="total_price" readonly="" value="30" name="total_price" class="form-control" placeholder="Total Price">
                      </div>
                    </div>
                </fieldset>
                <h3 class="text-center">Fill up the requirments.</h3>
                <hr>
                <h4>Personal Information</h4>
                <section class="pt-2">
                    <fieldset>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Passport Number</label>
                        <div class="col-sm-7">
                          <input id="passport_no" class="form-control" type="text" name="passport_no" placeholder="Passport No." value="">                    </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Full Name</label>
                        <div class="col-sm-7">
                        <input id="fullname" class="form-control" type="text" name="fullname" placeholder="Your Name*" value="" required="required">                    </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Email ID</label>
                        <div class="col-sm-7">
                        <input id="email" class="form-control" type="email" name="email" placeholder="Your Email*" value="" required="required">                    </div>
                      </div>
                    
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Mobile Number</label>
                        <div class="col-sm-7">
                        <input id="mobile" class="form-control" type="number" name="mobile" placeholder="Mobile No." value="" required="required">                    </div>
                      </div>
                    </fieldset>
                </section>
                <h4>Airport Pickup</h4>
                <section class="pt-2">
                    <fieldset>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Airport Pickup</label>
                        <div class="col-sm-7">
                        <div class="radio">
                        <input type="hidden" name="airport_pickup" value="">
                        <input id="airport_pickup-1" class="" title="" type="radio" name="airport_pickup" value="Yes" required="required">
                        <label for="airport_pickup-1">Yes</label>
                        &nbsp;&nbsp;
                        <input id="airport_pickup-2" class="" title="" type="radio" name="airport_pickup" value="No" required="required">
                        <label for="airport_pickup-2">No</label>                    </div>
                        </div>
                      </div>
                      <div class="form-group row mt-2">
                        <label class="col-form-label col-sm-3">Flight Number</label>
                        <div class="col-sm-7">
                        <input id="flight_no" class="datepicker form-control hasDatepicker" type="text" name="flight_no" placeholder="Flight Number" value="" required="required">                    </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3 ">Arrival Date</label>
                        <div class="col-sm-7">
                       <input type="text" class="datepicker form-control" name="arrivaldate" placeholder="Arrival Date">                    </div>
                      </div>
                      <!--
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Arrival Time</label>
                        <div class="col-sm-7">
                        <input id="arrival_time" class="timepicker form-control" type="text" name="arrival_time" placeholder="Arrival Time" value="" required="required">                    </div>
                      </div>
                    -->
                    </fieldset>
                </section>

                <h4>Additional Information</h4>
                <section class="pt-2">
                    <fieldset>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Check In Date</label>
                        <div class="col-sm-7">
                       <input type="text" class="datepicker form-control" name="checkin" placeholder="Checkin Date">                   </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Check Out Date</label>
                        <div class="col-sm-7">
                        <input type="text" class="datepicker form-control" name="checkout" placeholder="Checkout Date"></div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Number of Adults</label>
                        <div class="col-sm-7">
                        <select id="adults" name="adults" class="form-control"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>                    </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Number of Children</label>
                        <div class="col-sm-7">
                        <select id="children" name="children" class="form-control"><option value="0" selected="selected">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option></select>                    </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-form-label col-sm-3">Extra Requirements</label>
                        <div class="col-sm-7">
                        <textarea id="extra_requirement" class="form-control" name="extra_requirement" placeholder="Extra Requirements" rows="3" required="required"></textarea>                    </div>
                      </div>
                      <div class="checkbox row">
                        <label class="col-form-label col-sm-3"></label>
                        <div class="col-sm-7">
                          <label>
                            <label for="terms_conditions"></label><input type="hidden" name="terms_conditions" value=""><input type="checkbox" id="terms_conditions" class="checkbox-custom" name="terms_conditions" value="checked" required="required"> I agree the <a href="https://www.kathmandusunnyhotel.com/terms-and-conditions/" target="_blank">Terms and Conditions</a> of the HotelDeSpeigel
                          </label>
                        </div>
                        
                      </div>
                      <button type="submit" class="btn btn-primary">Submit</button>
                    </fieldset>
                </section>  

              </form>
            </div>
          </section>



      <div class="mt-5"></div>


      <script src="{{ asset('js/app.js') }}"></script>

      <script>
        
        $(document).ready(function() {
            $( ".datepicker" ).datepicker();
            
        });


      </script>

      <script>
        
        $(document).ready(function() {
            $(window).scroll(function() {
                if ($(document).scrollTop() > 50) {
                    $('nav').addClass('shrink');
                }
                else {
                    $('nav').removeClass('shrink');
                }
            });
        });


      </script>

  

    {{--   <script>

      use datetimepicker if time is needed for arrival
        $(document).ready(function(){
           $('.timepicker').timepicker({
            hourGrid: 4,
            minuteGrid: 10,
            timeFormat: 'hh:mm tt'
           });
        });
      </script> --}}


    </body>  
</html>
