<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>HotelDeSpiegel</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">


    </head>
   


        <body>



            <!--          navbar start -->
            
              @include('include.navbar')  
        
          <!--          navbar end -->





          <!--          carousel start -->
        
           <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
             <div class="carousel-inner">
               <div class="carousel-item active">
                 <img class="d-block w-100" src="img/slider/bg-1.jpg" alt="First slide">
               </div>
               <div class="carousel-item">
                 <img class="d-block w-100" src="img/slider/bg-2.jpg" alt="Second slide">
               </div>
               <div class="carousel-item">
                 <img class="d-block w-100" src="img/slider/bg-3.jpg" alt="Third slide">
               </div>
             </div>
             <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
               <span class="carousel-control-prev-icon" aria-hidden="true"></span>
               <span class="sr-only">Previous</span>
             </a>
             <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
               <span class="carousel-control-next-icon" aria-hidden="true"></span>
               <span class="sr-only">Next</span>
             </a>
           </div>
            
              <!--          carousel end -->



              <!--    booking start    -->
    
            <div class="container-fluid" id="booking">
                <div class="row mt-3">
                  <div class="col-md-8 offset-md-2">
                       
                      <form method="GET" action="{{ url('/search') }}" class="form-inline">
                        <!-- {{ csrf_field() }} -->
                        @csrf
                        <ul class="list-inline">
                          <li class="list-inline-item"> 
                            <span class="ml-5">Check-in Date</span>
                            <div class="input-group">  
                              <input type="text" class="datepicker form-control" name="checkin" placeholder="checkin date">
                            </div>
                          </li>
                           <li class="list-inline-item ">
                            <span class="ml-5">Check-out Date</span>
                            <div class="input-group ">
                              <input type="text" class="datepicker form-control" name="checkout" placeholder="checkout date">
                            </div>
                          </li>
                          <li class="list-inline-item ">
                              <span class="ml-5">Room</span>
                              <div class="input-group ">
                                
                                <select class="custom-select" name="person">
                                  <option selected>Number of room</option>
                                  <option value="1">One</option>
                                  <option value="2">Two</option>
                                  <option value="3">Three</option>
                                </select>
                              </div>
                          </li>
                        </ul>
                       
                        <button type="submit" class="btn btn-primary">Book Now</button>
                      </form>
                </div>
              </div>
            </div>

              <!--    booking start    -->
          
        </body>


      <script src="{{ asset('js/app.js') }}"></script>
      <!-- <script src=""></script> -->
      
      <script>
        $(document).ready(function(){
            $( ".datepicker" ).datepicker();
        });
      </script>
</html>
