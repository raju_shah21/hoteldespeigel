<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>HotelDeSpiegel</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
       
      
    </head>
  
    <body class="searchpage">
    
<!-- 
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">
          <img src="img/hotel-logo.jpg" width="40px" height="80px;"></a>
        <button class="navbar-toggler mr-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse mr-5" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="#">Room <span class="sr-only">(current)</span></a>
            </li>
           
          {{--   <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> --}}
         
          </ul>
         
        </div>
      </nav> -->




        <!--          navbar start -->
      
           @include('include.navbar')  

      <!--          navbar end -->





      <!--          hote image start -->
      
      <div class="inner-banner">
        <img src="img/slider/bg-1.jpg" alt="" width="100%" height="350px">

        <div class="inner-caption">
              <h1>All Room Types</h1>
              <p>HotelDeSpeigel offers a variety of room types for you. Our guests can simply look down below to learn about the price, availability and features of their desired rooms.</p>
            </div>
      </div>
        
          <!--          hotel image end -->


      

      <!--      room rows     start   -->
      <div class="container mt-5" id="roomrow" >
              <div class="row pt-4 pb-4">
                <div class="col">
                  <div class="room-type-image">
                    <a href="">
                    <img src="img/rooms/1.jpg" alt="" title="queebtn-primaryn-bed-with-ac">              </a>
                  </div>
                </div>
              
                <div class="col">
                  <div class="room-feature">
                  
                    <ul>
                    <li><i class="fas fa-check"></i>Free Wifi</li>
                    <li><i class="fas fa-check"></i>No Smoke</li>
                    <li><i class="fas fa-check"></i>Room telephone services</li>
                    <li><i class="fas fa-check"></i>Air conditioning</li>
                    <li><i class="fas fa-check"></i>Queen/Twin bed</li>
                    <li><i class="fas fa-check"></i>Private Balcony</li>
                    </ul>
                  </div>
                </div>
                <div class="col">
                  <div class="room-price">
                    <span>Price for 1 night</span>
                    <h2>$30</h2>
                    <span>Non Refundable</span>
                  </div>
                </div>
                <div class="col">
                  <div id="available">
                    <div class="badge badge-success btn-sm p-2">Available</div><br>
                    <div class="mt-3 mb-3">This room is now available. So, you can book it.</div>
                    <br>
                    <a href="{{ route('book') }}" class="btn btn-primary">Book Now</a>
                  </div>
                </div>
              </div>
            </div>


      <div class="container mt-5" id="roomrow" >
        <div class="row pt-4 pb-4">
          <div class="col">
            <div class="room-type-image">
              <a href="">
              <img src="img/rooms/1.jpg" alt="" title="queebtn-primaryn-bed-with-ac">              </a>
            </div>
          </div>
        
          <div class="col">
            <div class="room-feature">
            
              <ul>
              <li><i class="fas fa-check"></i>Free Wifi</li>
              <li><i class="fas fa-check"></i>No Smoke</li>
              <li><i class="fas fa-check"></i>Room telephone services</li>
              <li><i class="fas fa-check"></i>Air conditioning</li>
              <li><i class="fas fa-check"></i>Queen/Twin bed</li>
              <li><i class="fas fa-check"></i>Private Balcony</li>
              </ul>
            </div>
          </div>
          <div class="col">
            <div class="room-price">
              <span>Price for 1 night</span>
              <h2>$30</h2>
              <span>Non Refundable</span>
            </div>
          </div>
          <div class="col">
            <div id="available">
              <div class="badge badge-warning btn-sm p-2">UnAvailable</div><br>
              <div class="mt-3 mb-3">This room is now available. So, you can book it.</div>
              <br>
              <a href="{{ route('book') }}" class="btn btn-primary disabled">Book Now</a>
            </div>
          </div>
        </div>
      </div>

        <!--      room rows     end   -->

      <div class="mt-5"></div>


      <script src="{{ asset('js/app.js') }}"></script>

      <script>
        
        $(document).ready(function() {
            $(window).scroll(function() {
                if ($(document).scrollTop() > 50) {
                    $('nav').addClass('shrink');
                }
                else {
                    $('nav').removeClass('shrink');
                }
            });
        });


      </script>


    </body>  
</html>
